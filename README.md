# SheboyganJS - Deploying a JS Project

Meetup Jun 16, 2021

Deploying a JS project to AWS (URL Shortener)

- [ Tiny (Project Description) ](#tiny-project-description)
- [ Download and Configuration Links ](#download-and-configuration-links)
  - [ NodeJS ](#nodejs)
  - [ AWS Account (required) ](#aws-account-required)
  - [ AWS CLI ](#aws-cli)
  - [ AWS Elastic Beanstalk CLI ](#aws-elastic-beanstalk-cli)
- [ Running Locally ](#running-locally)
  - [ Clone the Repo ](#clone-the-repo)
  - [ Client ](#client)
  - [ API ](#api)
- [ Deploying the Client to AWS S3 ](#deploying-the-client-to-aws-s3)
  - [ Create an S3 bucket for the Client ](#create-an-s3-bucket-for-the-client)
  - [ AWS S3API ](#aws-s3api)
  - [ Bucket Block Public Access ](#bucket-block-public-access)
  - [ Copy Client source files to the bucket ](#copy-client-source-files-to-the-bucket)
  - [ Enable Static Website Hosting ](#enable-static-website-hosting)
  - [ Bucket Policy ](#bucket-policy)
  - [ View The Website Online ](#view-the-website-online)
- [ Deploying the API to AWS Elastic Beanstalk ](#deploying-the-api-to-aws-elastic-beanstalk)
  - [ Supported Environments ](#supported-environments)
    - [ Supported application languages ](#supported-application-languages)
    - [ Supported runtimes ](#supported-runtimes)
  - [ Create Key-Pair ](#create-key-pair)
  - [ Create a new Application ](#create-a-new-application)
  - [ Add Procfile Configuration ](#add-procfile-configuration)
  - [ Create a new Environment ](#create-a-new-environment)
  - [ Set Environment Variables ](#set-environment-variables)
  - [ If serving static files Open the Environment ](#if-serving-static-files-open-the-environment)
  - [ Deploy updated source code to the Environment ](#deploy-updated-source-code-to-the-environment)
- [ Adding a Domain with Route53 ](#adding-a-domain-with-route53)
  - [ Create S3 Alias Record ](#create-s3-alias-record)
  - [ Create Elastic Beanstalk Alias Record ](#create-elastic-beanstalk-alias-record)
- [ Further Reading ](#further-reading)
  - Securing with TLS (S3/CloudFront)
  - Securing with TLS (Elastic Beanstalk)
  - Blue/Green Deployments (eb swap)

<br>
<br>
<br>

## Tiny (Project Description)

- Purpose: Demo deploying a JS project to AWS
- Project: A simple URL Shortener
- Components
  - Client: simple ui providing inputs and request handlers
  - API: simple express server providing an in-memory { key: value } store

<br>
<br>
<br>

## Download and Configuration Links

### NodeJS
- [ Download ](https://nodejs.org/en/download/)

### AWS Account (required)
- [ Console Homepage ](https://aws.amazon.com/console/)

### AWS CLI
- [ Download ](https://docs.aws.amazon.com/cli/latest/userguide/install-cliv2.html)
- [ Configure ](https://docs.aws.amazon.com/cli/latest/userguide/cli-configure-quickstart.html) - `aws configure`
  - To create AWS Access Key ID
    - Click on username (top right)
    - My security credentials
    - Access keys
  - Default region: us-east-2 (Ohio)
  - Default output: json | yaml

### AWS Elastic Beanstalk CLI
- [ Download ](https://github.com/aws/aws-elastic-beanstalk-cli-setup)

<br>
<br>
<br>

## Running Locally

### Clone the Repo

```
git clone https://gitlab.com/EleMint/sheboyganjs.git
```

or using SSH

```
git clone git@gitlab.com:EleMint/sheboyganjs.git
```

### Client

```
cd ./s3_local
python -m http.server 8080

OR

python3 -m http.server 8080
```

### API

```
cd ./eb
npm install
npm start
```

<br>
<br>
<br>

## Deploying the Client to AWS S3

### Create an S3 bucket for the Client


```
aws s3api create-bucket --bucket sheboyganjs.com --region us-east-2 --create-bucket-configuration LocationConstraint=us-east-2
```
Note: make a copy from this bucket prefixed with `www.` (static website hosting to the base bucket object)

### AWS S3API

- [ Reference ](https://docs.aws.amazon.com/cli/latest/reference/s3api/)

### Bucket Block Public Access

- Ensure `Block all public access` is disabled
- Accept the public access acknowledgement

### Copy Client source files to the bucket

```
aws s3 cp ./s3 s3://sheboyganjs.com --recursive
```

or upload the folder directly

### Enable Static Website Hosting

- S3 Bucket -> Properties -> Static website hosting
- Hosting type: `Host a static website`
- Index document: `index.html`

### Bucket Policy

- S3 Bucket -> Permissions -> Bucket policy
- [ S3 Bucket Policy Reference ](https://docs.aws.amazon.com/AmazonS3/latest/userguide/access-policy-language-overview.html)
- Copy and Paste [ ./etc/aws_bucket_policy.jsonc ](https://gitlab.com/EleMint/sheboyganjs/-/blob/main/etc/aws_bucket_policy.jsonc)

### View The Website Online

- S3 Bucket -> Properties -> Bucket website endpoint
- Example: [ http://sheboyganjs.com.s3-website.us-east-2.amazonaws.com ](http://sheboyganjs.com.s3-website.us-east-2.amazonaws.com)

<br>
<br>
<br>

## Deploying the API to AWS Elastic Beanstalk

### Supported Environments
#### Supported application languages
  - Java
  - .NET
  - PHP
  - Node.js
  - Python
  - Ruby
  - Go
  - Docker

#### Supported runtimes
  - Apache
  - Nginx
  - Passenger
  - IIS

### Create Key-Pair

[ EC2 Link ](https://console.aws.amazon.com/ec2/v2/home?region=us-east-2#KeyPairs:)

### Create a new Application

```
cd ./eb
eb init --platform node.js --region us-east-2
```

- Select a default region: 14 // us-east-2
- Enter Application Name: sheboyganjs.com
- It appears you are using Node.js. Is this correct? Y
- Select a platform branch: 1 // Node.js 14 running on 64bit Amazon Linux 2
- Do you want to set up SSH for your instances: Y
- Select a keypair: sheboyganjs // created above

### Add Procfile Configuration

```
echo web: npm start >> Procfile
```

- `./Procfile` is used by Elastic Beanstalk as the start command
- By default if `./Procfile` is not provided Elastic Beanstalk will run `npm start`

### Create a new Environment

```
eb create --sample sheboyganjs-dev
```

- Enter Environment Name: sheboyganjs-dev
- Enter DNS CNAME prefix: // leave as default (same as environment)
- Select a load balancer type: 2 // application
  - classic: the previous-generation Elastic Beanstalk load balancer
  - application: an Application layer load balancer
  - network: a network layer load balancer
- Would you like to enable Spot Fleet requests?: N
  - Spot Fleet configures AWS to attempt maintaining a tartget capacity
  - IAM Roles
  - Specify EC2 AMI
  - Key-Pairs
  - Instance Type

### Set Environment Variables

```
eb setenv PORT=80
```

- By default the Elastic Beanstalk load balancer forwards port 80

### If serving static files Open the Environment

```
eb open
```

### Deploy updated source code to the Environment

```
eb deploy
```

### If needed SSH into an instance

```
eb ssh

or

eb ssh -e "ssh -i ../etc/example_key_pair.pem"
```

<br>
<br>
<br>

## Adding a Domain with Route53

### Purchase Domain and Transfer to Route53
Or find a domain name on [ Route53 ](https://console.aws.amazon.com/route53/home?#DomainRegistration:)

### Create S3 Alias Record

- Route53 -> Hosted zones -> sheboyganjs.com -> Create Record
- Simple routing
- Define simple record
- Record name: // leave empty
- Record type: A
- Value/Route traffic to: Alias to S3 website endpoint
- Region: us-east-2
- Endpoint: sheboyganjs.com
  - Make another for `www.` with the same options

### Create Elastic Beanstalk Alias Record

- Route53 -> Hosted zones -> sheboyganjs.com -> Create Record
- Simple routing
- Define simple record
- Record name: api
- Record type: A
- Value/Route traffic to: Alias to Elastic Beanstalk environment
- Region: us-east-2
- Environment: sheboyganjs-dev.eba-mjcmddxn.us-east-2.elasticbeanstalk.com

<br>
<br>
<br>

## Further Reading

- [ Securing with TLS (S3/CloudFront) ](https://docs.aws.amazon.com/AmazonCloudFront/latest/DeveloperGuide/using-https-viewers-to-cloudfront.html)
- [ Securing with TLS (Elastic Beanstalk) ](https://docs.aws.amazon.com/elasticbeanstalk/latest/dg/configuring-https.html)
- [ Blue/Green Deployments (eb swap) ](https://docs.aws.amazon.com/elasticbeanstalk/latest/dg/using-features.CNAMESwap.html)
  - When making an update to an environment, Elastic Beanstalk removes the service before starting the updated environment
  - Services during this peroid can become unavailable
  - By upgrading a staging environment to the latest version and using `eb swap` deployments become seamless
