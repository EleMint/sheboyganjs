const redirect = (tag) => {
  fetch(
    `http://sheboyganjs-dev.eba-mjcmddxn.us-east-2.elasticbeanstalk.com/${tag}`,
    {
      method: "GET",
    }
  )
    .then((resp) => resp.json())
    .then((resp) => {
      const { url } = resp;
      if (url) {
        document.location.assign(url);
      }
    })
    .catch((err) => alert(err));
};

const submit = (tag, url) => {
  fetch("http://sheboyganjs-dev.eba-mjcmddxn.us-east-2.elasticbeanstalk.com/", {
    method: "POST",
    headers: {
      "Content-Type": "application/json",
    },
    body: JSON.stringify({
      tag,
      url,
    }),
  })
    .then((resp) => {
      switch (resp.status) {
        case 200:
          alert(`Tag '${tag}' successfully created to '${url}'`);
        case 409:
          alert("conflict");
          return;
        default:
          alert("request failed");
      }
    })
    .catch((err) => console.error(err));
};

const tag = document.getElementById("tag");
const url = document.getElementById("url");
const submit_btn = document.getElementById("submit");
const tagRedirect = document.getElementById("tag-redirect");
const redirect_btn = document.getElementById("redirect");

const getTagValue = () => tag.value;
const getUrlValue = () => url.value;
const getTagRedirectValue = () => tagRedirect.value;

submit_btn.addEventListener("click", (e) => {
  e.preventDefault();

  submit(getTagValue(), getUrlValue());
});

redirect_btn.addEventListener("click", (e) => {
  e.preventDefault();

  redirect(getTagRedirectValue());
});
