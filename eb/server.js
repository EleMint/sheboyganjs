const express = require("express");
const app = express();
const port = process.env.PORT || 3000;

app.use(function (req, res, next) {
  res.header("Access-Control-Allow-Origin", "*");
  res.header("Access-Control-Allow-Credentials", true);
  res.header("Access-Control-Allow-Methods", "POST, GET, OPTIONS");
  res.header(
    "Access-Control-Allow-Headers",
    "Origin, X-Requested-With, Content-Type, Accept"
  );
  next();
});
app.use(express.json());

const datastore = new Map();

app.post("/", (req, res) => {
  const { tag, url } = req.body;

  if (datastore.has(tag)) {
    res.writeHead(409, "conflict");
    res.end();
    return;
  }

  datastore.set(tag, url);
  res.writeHead(200, "ok");
  res.end();
});

app.get("/:tag", (req, res) => {
  const { tag } = req.params;

  if (datastore.has(tag)) {
    res.status(200).json({ url: datastore.get(tag) });
    res.end();
    return;
  }

  res.writeHead(404, "not found");
  res.end();
});

app.listen(port, () => {
  console.log(`URL Shortener listening on http://localhost:${port}`);
});
